# Build
FROM node:12 AS builder

RUN git clone https://github.com/angular/material.angular.io.git /material.angular.io

WORKDIR /material.angular.io

RUN yarn
RUN yarn build
RUN yarn build:content
RUN yarn prod-build
RUN ls /material.angular.io

# Serve
FROM nginx:alpine

COPY --from=builder /material.angular.io/dist /usr/share/nginx/html
